import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PSU Trang",
      home: Scaffold(
        appBar: AppBar(
          title: Text('My First App'),

        ),
        body: Column (

          children: [
            Image.asset('assets/images/name.jpg',width: 600, height: 400,fit: BoxFit.cover),
            Text('นาย ศุทธิพัฒน์ วัฒนสิน',style:TextStyle(height: 5, fontSize: 30)),
            Text('6350110028',style:TextStyle(fontSize: 30)),
          ],

        ),
      ),
    );
  }
}

